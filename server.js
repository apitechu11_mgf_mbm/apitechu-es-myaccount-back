console.log("inicializamos la aplicación");

const express = require('express'); // representa nuestro framework que usaremos en el proyecto
const app = express();  // inicializamos el framework de express
app.use(express.json());  // indicamos al framework de express que procese las peticiones del body como json


require('dotenv').config(); // variables de entorno - configuración

const config = require('./libs/config');

// módulos de nuestra API
const jwtController = require('./controllers/JwtController');
const authController = require('./controllers/AuthController');
const userController = require('./controllers/UserController');
const accountController = require('./controllers/AccountController');
const movController = require('./controllers/MovController');
const opeController = require('./controllers/OpeController');
const EmailCtrl = require('./controllers/notificacionEmail');

var enableCORS = function (req, res, next) {
  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
  res.set("Access-Control-Allow-Headers", "Content-Type,jwt");

  next();
}

app.use(enableCORS); // activamos el access-control a permitir como parte de CORS

app.listen(config.port); // se pone a la escucha en el puerto definido

console.log("API escuchando de nuevo en el puerto " + config.port);

app.post(config.base_url   + '/login',                  authController.login);      // Login de usuario
app.post(config.base_url   + '/users',                  userController.createUser); // Alta de usuario
app.post(config.base_url   +'/forgetmail',              EmailCtrl.sendForget);      // Reseteo de PWD

app.post(config.base_url   + '/logout/:userid',         jwtController.validateJwt, authController.logout); // Logout de usuario

app.get(config.base_url    + '/users',                  jwtController.validateJwt, userController.getUsers); // Consulta de usuarios con filtros
app.get(config.base_url    + '/users/:userid',          jwtController.validateJwt, userController.getUsersById); // Consulta de usuario por id
app.delete(config.base_url + '/users/:userid',          jwtController.validateJwt, userController.deleteUserById); // Baja de usuario por id

app.get(config.base_url    + '/accounts',               jwtController.validateJwt, accountController.getAccounts); // Consulta de cuentas con filtros (sólo para empleados)

app.post(config.base_url   + '/accounts/:userid',       jwtController.validateJwt, accountController.createAccount); // Alta de cuenta
app.get(config.base_url    + '/accounts/:userid',       jwtController.validateJwt, accountController.getAccountsById); // Consulta cuentas de un usuario por su id
app.delete(config.base_url + '/accounts/:userid/:iban', jwtController.validateJwt, accountController.deleteAccountById); // Baja de una cuenta por id

app.get(config.base_url    + '/movement/:userid/:iban', jwtController.validateJwt, movController.consultarMov); // Busqueda de movimientos

app.post(config.base_url   + '/ingreso/:userid',        jwtController.validateJwt, opeController.ingresoMov);   // Alta de movimientos - tipo ingreso
app.post(config.base_url   + '/reintegro/:userid',      jwtController.validateJwt, opeController.reintegroMov); // Alta de movimientos - tipo reintegro
app.post(config.base_url   + '/transfer/:userid',       jwtController.validateJwt, opeController.transferMov);  // Alta de movimientos - tipo transferencia


app.get(config.base_url + '/ping',
  function(req, res){
    console.log('GET ' + config.base_url + '/ping');
    res.send({"msg": config.base_url + "/ping OK"});
  }
);
