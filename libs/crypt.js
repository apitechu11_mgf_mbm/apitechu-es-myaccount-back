const bcrypt = require('bcrypt');
const crypto = require('crypto-js');
const config = require('./config');

function hash(data) {
  console.log("Hashing data");
  return bcrypt.hashSync(data, 10);
}

function checkPassword(pwdUserPlainText,pwdUserHashed) {
  console.log("Checking password");
  return bcrypt.compareSync(pwdUserPlainText, pwdUserHashed); // true
}

function encryptStringAES (data) {
  var encrypted = crypto.AES.encrypt(JSON.stringify(data), config.aes_secret).toString();
  return crypto.enc.Base64.stringify(crypto.enc.Utf8.parse(encrypted));
}

function decryptStringAES (data) {
    var base64Text = crypto.enc.Base64.parse(data.toString());
    var utf8Text = base64Text.toString(crypto.enc.Utf8);

    var decrypted = crypto.AES.decrypt(utf8Text, config.aes_secret);
    return JSON.parse(decrypted.toString(crypto.enc.Utf8));
};

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
module.exports.encryptStringAES = encryptStringAES;
module.exports.decryptStringAES = decryptStringAES;

