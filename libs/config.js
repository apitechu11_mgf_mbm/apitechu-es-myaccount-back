
const _port = process.env.PORT || 3000; // definimos el puerto en el que va a estar escuchando nuestra aplicación node.js

const base_api = 'techu'; // definimos la version de nuestra API
const version_api = 'v1'; // definimos la version de nuestra API
const _base_url = '/' + base_api + '/' + version_api; // definimos la base de nuestra API

const _creador = 'apiTechU11ed'; // creador del jwt de esta aplicación
const _jwt_secret = process.env.CRYPTO_JWT_SECRET;
const _aes_secret = process.env.CRYPTO_AES_SECRET;

const _mlab = {
    database_uri: 'https://api.mlab.com/api/1/databases/' + process.env.MLAB_ID_CONTAINER + '/collections/',
    database_apikey: 'apiKey=' + process.env.MLAB_API_KEY,
    database_fieldId: 'f={"_id":0}'
};

const _mailserver = process.env.EMAIL_USER
const _passserver = process.env.EMAIL_PASSWORD

module.exports.port = _port;
module.exports.base_url = _base_url;
module.exports.creador = _creador;
module.exports.jwt_secret = _jwt_secret;
module.exports.aes_secret = _aes_secret;
module.exports.mlab = _mlab;
module.exports.mailserver = _mailserver;
module.exports.passserver = _passserver;