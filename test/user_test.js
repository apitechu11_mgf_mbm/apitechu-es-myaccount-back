//'use strict';
var mocha = require('mocha');
var chai = require('chai');
var chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();

const hostName = 'http://localhost:3000';
const apiContext = '/techu/v1';

// Lanzar el server
var server = require('../server');

describe("Test de API V1 de Users",
  function() {

    var id_usuario_emp = "";
    var id_jwt_emp = "";
    var id_usuario_cust = "";
    var id_jwt_cust = "";
    var id_usuario_baja = "";
    var user_iban = "";
    var user_iban_ok ="ES63 0182 4000 6459 3371 7563";

    // Login de usuario - OK (EMPLOYEE)
    it('Login de usuario - OK (EMPLOYEE)',
      function(done) {
        chai.request(hostName)
        .post(apiContext + '/login')
        .set('content-type', 'application/json')
        .send(
          {
            "email": "marco.barreche@apitechu.com",
            "password": "0123456"
          })
        .end(
          function(err,res) {
            console.log("Request has ended");
            console.log('err:' + err);
            console.log('body:' + res.body);

            res.should.have.status(200);
            res.should.be.json;
            res.body.should.have.property('id');
            res.body.should.have.property('type');
            res.body.type.should.be.eql("EMPLOYEE");
            res.body.should.have.property('jwt');
            console.log (res.body.id + '/'+ res.body.jwt);
            id_usuario_emp = res.body.id;
            id_jwt_emp = res.body.jwt;
            console.log ('id_usuario_emp --> ' + id_usuario_emp);
            console.log ('id_jwt_emp --> ' + id_jwt_emp);
            done();
          }
        )
      }
    ),

    // Login de usuario - OK (CUSTOMER)
    it('Login de usuario - OK (CUSTOMER)',
      function(done) {
        chai.request(hostName)
        .post(apiContext + '/login')
        .set('content-type', 'application/json')
        .send(
          {
            "email": "mrc.barreche@gmail.com",
            "password": "0123456"
          })
        .end(
          function(err,res) {
            console.log("Request has ended");
            console.log('err:' + err);
            console.log('body:' + res.body);

            res.should.have.status(200);
            res.should.be.json;
            res.body.should.have.property('id');
            res.body.should.have.property('type');
            res.body.type.should.be.eql("CUSTOMER");
            res.body.should.have.property('jwt');
            console.log (res.body.id + '/'+ res.body.jwt);
            id_usuario_cust = res.body.id;
            id_jwt_cust = res.body.jwt;
            console.log ('id_usuario_cust --> ' + id_usuario_cust);
            console.log ('id_jwt_cust --> ' + id_jwt_cust);
            done();
          }
        )
      }
    ),

    // Login de usuario - pwd errónea
    it('Login de usuario - pwd errónea',
      function(done) {
        chai.request(hostName)
        .post(apiContext + '/login')
        .set('content-type', 'application/json')
        .send(
          {
            "email": "marco.barreche@apitechu.com",
            "password": "1111111"
          })
        .end(
          function(err,res) {
            console.log("Request has ended");
            console.log('err:' + err);
            console.log('body:' + res.body);

            res.should.have.status(401);
            res.should.be.json;
            res.body.msg.should.be.eql("Usuario no validado");
            console.log (res.body.msg);
            done();
          }
        )
      }
    ),

    // Login de usuario - usuario inexistente
    it('Login de usuario - usuario inexistente',
      function(done) {
        chai.request(hostName)
        .post(apiContext + '/login')
        .set('content-type', 'application/json')
        .send(
          {
            "email": "no.existe.usuario@apitechu.com",
            "password": "1111111"
          })
        .end(
          function(err,res) {
            console.log("Request has ended");
            console.log('err:' + err);
            console.log('body:' + res.body);

            res.should.have.status(401);
            res.should.be.json;
            res.body.msg.should.be.eql("Usuario no validado");
            console.log (res.body.msg);
            done();
          }
        )
      }
    ),

    // Logout de usuario - no jwt
    it('Logout de usuario - no jwt',
      function(done) {
        chai.request(hostName)
        .post(apiContext + '/logout/' + id_usuario_cust)
        .set('content-type', 'application/json')
        .send({})
        .end(
          function(err,res) {
            console.log("Request has ended");
            console.log('err:' + err);
            console.log('body:' + res.body);

            res.should.have.status(403);
            res.should.be.json;
            res.body.msg.should.be.eql("Token jwt no informado");
            done();
          }
        )
      }
    ),

    // Logout de usuario - OK
    it('Logout de usuario - OK',
      function(done) {
        chai.request(hostName)
        .post(apiContext + '/logout/' + id_usuario_cust)
        .set('content-type', 'application/json')
        .set('jwt', id_jwt_cust)
        .send({})
        .end(
          function(err,res) {
            console.log("Request has ended");
            console.log('err:' + err);
            console.log('body:' + res.body);

            res.should.have.status(200);
            res.should.be.json;
            res.body.should.have.property('id');
            res.body.msg.should.be.eql("Usuario deslogado");
            done();
          }
        )
      }
    ),

    // Logout de usuario - no logado
    it('Logout de usuario - no logado',
      function(done) {
        chai.request(hostName)
        .post(apiContext + '/logout/' + id_usuario_cust)
        .set('content-type', 'application/json')
        .set('jwt', id_jwt_cust)
        .send({})
        .end(
          function(err,res) {
            console.log("Request has ended");
            console.log('err:' + err);
            console.log('body:' + res.body);

            res.should.have.status(409);
            res.should.be.json;
            res.body.msg.should.be.eql("Usuario no logado");
            done();
          }
        )
      }
    ),

    // Alta de usuario - OK
    it('Alta de usuario - OK',
      function(done) {
        chai.request(hostName)
        .post(apiContext + '/users')
        .set('content-type', 'application/json')
        .send(
          {
            "first_name": "Marco",
            "last_name":"Barreche",
            "email":"marco.barreche.2@apitechu.com",
            "password": "0123456",
            "type":"EMPLOYEE",
            "home": {
                "country": "España",
                "province": "Madrid",
                "location": "Madrid",
                "address": "C/ María Tubau, 10",
                "postal_code": 28050,
                "geolocation": {
                  "longitude": 40.5139126,
                  "latitude": -3.6742344
                }
              }
          })
        .end(
          function(err,res) {
            console.log("Request has ended");
            console.log('err:' + err);
            console.log('body:' + res.body);

            res.should.have.status(201);
            res.should.be.json;
            res.body.should.have.property('id');
            console.log (res.body.id);
            id_usuario_baja = res.body.id;
            console.log ('id_usuario_baja --> ' + id_usuario_baja);
            done();
          }
        )
      }
    ),

    // Alta de usuario - duplicado
    it('Alta de usuario - duplicado',
      function(done) {
        chai.request(hostName)
        .post(apiContext + '/users')
        .set('content-type', 'application/json')
        .send(
          {
            "first_name": "Marco",
            "last_name":"Barreche",
            "email":"marco.barreche.2@apitechu.com",
            "password": "0123456",
            "type":"EMPLOYEE",
            "home": {
                "country": "España",
                "province": "Madrid",
                "location": "Madrid",
                "address": "C/ María Tubau, 10",
                "postal_code": 28050,
                "geolocation": {
                  "longitude": 40.5139126,
                  "latitude": -3.6742344
                }
              }
          })
        .end(
          function(err,res) {
            console.log("Request has ended");
            console.log('err:' + err);
            console.log('body:' + res.body);

            res.should.have.status(409);
            res.should.be.json;
            res.body.msg.should.be.eql("Usuario existente");
            done();
          }
        )
      }
    ),

    // Baja de usuario - OK
    it('Baja de usuario - OK',
      function(done) {
        chai.request(hostName)
        .delete(apiContext + '/users/' + id_usuario_baja)
        .set('content-type', 'application/json')
        .set('jwt', id_jwt_emp)
        .send({})
        .end(
          function(err,res) {
            console.log("Request has ended");
            console.log('err:' + err);
            console.log('body:' + res.body);

            res.should.have.status(200);
            res.should.be.json;
            res.body.msg.should.be.eql("Usuario borrado");
            done();
          }
        )
      }
    ),

    // Baja de usuario - no existente
    it('Baja de usuario - no existente',
      function(done) {
        chai.request(hostName)
        .delete(apiContext + '/users/' + id_usuario_baja)
        .set('content-type', 'application/json')
        .set('jwt', id_jwt_emp)
        .send({})
        .end(
          function(err,res) {
            console.log("Request has ended");
            console.log('err:' + err);
            console.log('body:' + res.body);

            res.should.have.status(404);
            res.should.be.json;
            res.body.msg.should.be.eql("Usuario no encontrado");
            done();
          }
        )
      }
    ),

    // Obtener cuenta por Identificador - OK
    it ("Obtener cuentas por ID de usuario",function (done){ // cada it es un test unitario , done es la funcion manejadora,
      chai.request(hostName)
         .get(apiContext + '/accounts/'+id_usuario_cust) //lo que va despues del dominio, a donde voy
         .set('jwt', id_jwt_cust)
         .end(
           function (err, res){                               //función manejadora
             console.log("Solicitud cuentas por usuario terminada");
             res.should.have.status (200);
             res.should.be.json;
             done();
           }
         )
    }
  ),
    // Obtener cuenta por Identificador - KO
  //   it ("Obtener cuentas por ID de usuario KO",function (done){ // cada it es un test unitario , done es la funcion manejadora,
  //     chai.request(hostName)
  //        .get(apiContext + '/accounts/:userid') //lo que va despues del dominio, a donde voy
  //        .set('jwt', id_jwt_cust)
  //   //     .set('userid',id_usuario_cust)
  //        .end(
  //          function (err, res){                               //función manejadora
  //            console.log("Solicitud cuentas por usuario erronea");
  //            res.should.have.status (404);
  //       //     res.should.be.json;
  //            res.body.msg.should.be.eql("msg: no hay cuentas");
  //            done();
  //          }
  //        )
  //   }
  // )
  //crear cuenta dado un ID de usuario
  it('Crear cuenta - OK',
    function(done) {
      chai.request(hostName)
      .post(apiContext + '/accounts/'+id_usuario_cust)
      .set('content-type', 'application/json')
      .set('jwt', id_jwt_cust)
      .send(
        {
          "alias": "Cuenta BBVA familia numerosa",
	        "entidad":"0182",
	        "oficina":"3000",
	        "divisa": "EUR"
        })
      .end(
        function(err,res) {
          console.log("cuenta creada");
          res.should.have.status(201);
          res.should.be.json;
          res.body.should.have.property('iban');
          console.log (res.body.iban);
          user_iban = res.body.iban;
          done();
        }
      )
  }
),
//hacer un ingreso
it('hacer ingreso - OK',
  function(done) {
    chai.request(hostName)
    .post(apiContext + '/ingreso/'+id_usuario_cust)
    .set('content-type', 'application/json')
    .set('jwt', id_jwt_cust)
    .send(
      {
        "iban": "ES63 0182 3000 4287 2176 6406",
        "Concepto":"Ingreso del test",
        "Importe":"10"
      })
    .end(
      function(err,res) {
        console.log("ingreso realizado");
        res.should.have.status(200);
        res.should.be.json;
        res.body.msg.should.be.eql("Ingreso realizado");
        done();
      }
    )
}
),
//hacer un ingreso KO
it('hacer ingreso - KO',
  function(done) {
    chai.request(hostName)
    .post(apiContext + '/ingreso/'+id_usuario_cust)
    .set('content-type', 'application/json')
    .set('jwt', id_jwt_cust)
    .send(
      {
        "iban": "ES63 0182 3000 4287 2176 6466",
        "Concepto":"Ingreso del test",
        "Importe":"10"
      })
    .end(
      function(err,res) {
        console.log("Cuenta no encontrada");
        res.should.have.status(404);
        res.should.be.json;
        res.body.msg.should.be.eql("Cuenta no encontrada");
        done();
      }
    )
}
),
// Obtener movimientos de una cuenta OK
it ("Obtener cuentas por ID de usuario e iban OK",function (done){ // cada it es un test unitario , done es la funcion manejadora,
  chai.request(hostName)
     .get(apiContext + '/movement/'+id_usuario_cust+ '/'+ user_iban_ok) //lo que va despues del dominio, a donde voy
     .set('jwt', id_jwt_cust)
     .end(
       function (err, res){                               //función manejadora
         console.log("Solicitud movimientos cuentas");
         res.should.have.status (200);
         res.should.be.json;
         done();
       }
     )
}
),
// Obtener movimientos de una cuenta ko
it ("Obtener cuentas por ID de usuario e iban KO",function (done){ // cada it es un test unitario , done es la funcion manejadora,
  chai.request(hostName)
     .get(apiContext + '/movement/'+id_usuario_cust+ '/'+ user_iban) //lo que va despues del dominio, a donde voy
     .set('jwt', id_jwt_cust)
     .end(
       function (err, res){                               //función manejadora
         console.log("Solicitud movimientos cuentas");
         res.should.have.status (404);
         res.body.msg.should.be.eql("Cuenta no encontrada");
         done();
       }
     )
}
),
// Baja de cuenta - OK
it('Baja de cuenta - OK',
  function(done) {
    chai.request(hostName)
    .delete(apiContext + '/accounts/'+id_usuario_cust+'/' + user_iban)
    .set('content-type', 'application/json')
    .set('jwt', id_jwt_cust)
    .send({})
    .end(
      function(err,res) {
        console.log("Solicitud de borrar cuenta fue enviada");
        console.log('err:' + err);
        console.log('body:' + res.body.msg);
        res.should.have.status(200);
        res.should.be.json;
    //    res.body.msg.should.be.eql("Cuenta eliminada");
        done();
      }
    )
  }
)
    // Detalle de usuario - KO

  }
);
