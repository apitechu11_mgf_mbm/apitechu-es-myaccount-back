// importar librarías & módulos
const config = require('../libs/config');
const crypt = require('../libs/crypt'); // representa nuestra librería de criptografía
const requestJson = require('request-json');
const jwtController = require('./JwtController');

function login(req, res){
  console.log('POST ' + config.base_url + '/login');
  console.log(" >> BODY:");
  console.log(req.body);

  var email = req.body.email;
  var query = 'q={"email":"' + email + '"}';
  console.log(" >> Consulta = " + query);

  var httpClient = requestJson.createClient(config.mlab.database_uri);
  console.log(" >> Cliente http creado");
  httpClient.get("user?" + query + "&" + config.mlab.database_apikey,
    function(err, resMLab, body) {
      // console.log(resMLab);
      console.log(body);
      var respuesta = {};
      if (body.length > 0) {
        console.log("comparar pwd introducida " + req.body.password + " y almacenada " + body[0].password);
        var logged = crypt.checkPassword(req.body.password, body[0].password);
        var msg = logged ? "Usuario validado" : "Usuario no validado";
        if (logged){
          respuesta.id = body[0].id;
          respuesta.type = body[0].type;
          respuesta.msg = msg;
          respuesta.jwt = jwtController.createToken({'userid':respuesta.id, 'username':req.body.email, 'usertype':respuesta.type})

          var putBody = '{"$set":{"logged":true}}';
          httpClient.put("user?" + query + "&" + config.mlab.database_apikey, JSON.parse(putBody),
            function(err, resMLab, body) {
              console.log(body);
              res.send(respuesta);
            }
          );
        } else {
          respuesta.msg = msg;
          res.status(401).send(respuesta);
        }
      } else {
        var msg = "Usuario no validado";
        respuesta.msg = msg;
        res.status(401).send(respuesta);
      }
    }
  );
}

function logout(req, res){
  console.log('POST ' + config.base_url + '/logout/:userid');
  console.log(" >> PARAMS:");
  console.log(req.params);

  var id = req.params.userid;
  var query = 'q={"id":"' + id + '"}';
  console.log(" >> Consulta = " + query);
  var userLogged = [];

  var httpClient = requestJson.createClient(config.mlab.database_uri);
  console.log(" >> Cliente http creado");
  httpClient.get("user?" + query + "&" + config.mlab.database_apikey,
    function(err, resMLab, body) {
      //console.log(resMLab);
      console.log(body);
      var respuesta = {};
      if (body.length > 0) {
        console.log("comparar si usuario logado");
        var msg = body[0].logged ? "Usuario deslogado" : "Usuario no logado";
        if (body[0].logged){
          respuesta.id = body[0].id;
          respuesta.msg = msg;

          // delete body[0].logged;
          // httpClient.put("user?" + query + "&" + config.mlab.database_apikey, body[0],

          var putBody = '{"$unset":{"logged":""}}';
          httpClient.put("user?" + query + "&" + config.mlab.database_apikey, JSON.parse(putBody),
            function(err, resMLab, body) {
              console.log(body);
              res.send(respuesta);
            }
          );
        } else {
          respuesta.msg = msg;
          res.status(409).send(respuesta);
        }
      } else {
        var msg = "Usuario no logado";
        respuesta.msg = msg;
        res.status(409).send(respuesta);
        //res.send(respuesta);
      }
    }
  );
}

module.exports.login = login;
module.exports.logout = logout;
