// importar librarías & módulos
const config = require('../libs/config');
const jwt = require('jsonwebtoken');

function validateJwt(req, res, next){
    console.log("validateJwt");
    var imcOK = false;

    var token = req.headers['jwt'];

    console.log("obtenemos el token +", token);

    if (token) {
        console.log('parametros->', req.params);
        console.log('userid-->', req.params.userid);
        if (req.params.userid == undefined) {
            // imcOK = false;
            imcOK = checkToken(token,req.params.userid);  // si el usuario es de tipo EMPLOYEE deja pasar
        } else {
            imcOK = checkToken(token,req.params.userid);
        }

        if (imcOK) {
            console.log('debemos hacer next');
            next();
        } else {
            var respuesta = {
                "msg": "Error de autenticación"
            }
            res.status(403).send(respuesta);
        }
    } else {
        var respuesta = {
            "msg": "Token jwt no informado"
        }
        res.status(403).send(respuesta);
    }
}

function createToken (datos_token) {
    var now = Date.now();
    console.log(config.mlab);

    var tokenData = {
        "iss": config.creador,
        "sub": config.base_url,
        "iat": Math.floor(now / 1000) - 30,
        "exp": Math.floor(now / 1000) + (60 * 60),
        "userid": datos_token.userid,
        "username": datos_token.username,
        "usertype": datos_token.usertype
    }
    console.log (tokenData);

    var token = jwt.sign(datos_token, config.jwt_secret);
    console.log (token);
    return token;
}

  // Implementation Control del canal cliente
function checkToken (token, userid){
    try {
      var decoded = jwt.verify(token, config.jwt_secret);
      console.log(JSON.stringify(decoded) + ' vs '+ userid);
      return decoded.usertype == "EMPLOYEE" ? true : decoded.userid == userid;
    } catch (e) {
      console.log('error al validar el token ->', e.message);
      return false
    }
}

module.exports.validateJwt = validateJwt;
module.exports.createToken = createToken;
