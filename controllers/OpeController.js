const config = require('../libs/config');
const crypt = require('../libs/crypt'); // representa nuestra librería de criptografía
const requestJson = require('request-json');


function ingresoMov(req, res) {
  console.log('POST ' + config.base_url + '/ingreso/:userid');
  console.log(" >> BODY:");
  console.log(req.body);
  //dan un IBAN, un comentario y el Importe
  let now= hoyFecha();
  console.log('La fecha actual es',now);

  var query = 'q={"iban": "'+ req.body.iban + '"}';
  console.log("La consulta es:" + query);

  var httpClient = requestJson.createClient(config.mlab.database_uri);  //inicializo mi url de conexión a la BD
  console.log("Busqueda de saldo del la cuenta");

  httpClient.get("account?" + query + "&" + config.mlab.database_fieldId + "&" + config.mlab.database_apikey, //aqui le indico la colección y la apikey
    function(err,resMLab, myaccount){          //para asegurar lo que estoy preguntando puedo hacer un console del resMLab
      if (err){
        var response = {
          "msg" : "Error obteniendo dados de la cuenta"
        }
        res.status(500);
      }else{ // actualizo el saldo

        if (myaccount.length > 0){
          console.log("Saldo de la cuenta:" + myaccount[0].balance);
          myaccount[0].balance = Number.parseInt(myaccount[0].balance) + Number.parseInt(req.body.Importe);
          console.log("Nuevo saldo de la cuenta:" + myaccount[0].balance);
          //query = 'q={"id" : ' + body[0].id +'}';
          var putBody = '{"$set":{"balance":' + myaccount[0].balance+'}}';

          httpClient.put("account?" + query + "&" + config.mlab.database_apikey, JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT) {
              console.log("PUT done");
              if (err){
                console.log(err);
                var response = {
                  "msg" : "Error gravando movimientos"
                }
              }else {

                var newMov ={
                  "IBAN": req.body.iban,
                  "Fecha": now,
                  "Concepto": req.body.Concepto,
                  "Importe": req.body.Importe,
                  "Saldo": myaccount[0].balance
                }
                console.log (newMov);

                httpClient.post ("movement?" + config.mlab.database_apikey, newMov,
                  function (err,resMLab, pasbody){
                    if (err){
                      console.log(err);
                      var response = {
                        "msg" : "Error grabando movimientos"
                      }
                    }else{
                      console.log("movimiento guardado");
                      //                  res.status(200).send({"msg": "ingreso creado y guardado"});
                    }
                  }
                );
              }
            }
          );
            var response = {  "msg" : "Ingreso realizado"}
          ///ahora actualizo los datos en movimientos
        }else{
          var response = {
            "msg" : "Cuenta no encontrada"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}

function reintegroMov(req, res) {
  console.log('POST ' + config.base_url + '/reintegro/:userid');
  console.log(" >> BODY:");
  console.log(req.body);

  //dado un IBAN, un comentario y un Importe , resto del saldo de la cuenta
  let now= hoyFecha();

  var query = 'q={"iban": "'+req.body.iban + '"}';
  console.log("La consulta es:" + query);

  var httpClient = requestJson.createClient(config.mlab.database_uri);  //inicializo mi url de conexión a la BD
  console.log("Busqueda de saldo del la cuenta");

  httpClient.get("account?" + query + "&" + config.mlab.database_fieldId + "&" + config.mlab.database_apikey, //aqui le indico la colección y la apikey
    function(err,resMLab, myaccount){          //para asegurar lo que estoy preguntando puedo hacer un console del resMLab
      if (err){
        var response = {
          "msg" : "Error obteniendo dados de la cuenta"
        }
        res.status(500);
      }else{ // actualizo el saldo
        if (myaccount.length > 0){
          console.log("Saldo de la cuenta:" + myaccount[0].balance);
          myaccount[0].balance = Number.parseInt(myaccount[0].balance) - Number.parseInt(req.body.Importe);

          console.log("Nuevo saldo de la cuenta:" + myaccount[0].balance);
          var putBody = '{"$set":{"balance":' + myaccount[0].balance+'}}';

          httpClient.put("account?" + query + "&" + config.mlab.database_apikey, JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT) {
              if (err){
                console.log(err);
                var response = {
                  "msg" : "Error grabando movimientos"
                }
              }else {
                var newMov ={
                  "IBAN": req.body.iban,
                  "Fecha": now,
                  "Concepto": req.body.Concepto,
                  "Importe": req.body.Importe,
                  "Saldo": myaccount[0].balance
                }

                httpClient.post ("movement?" + config.mlab.database_apikey, newMov,
                  function (err,resMLab, pasbody){
                    if (err){
                      console.log(err);
                      var response = {
                        "msg" : "Error grabando movimientos"
                      }
                    }
                  }
                )
              }
            }
          );
            var response = {  "msg" : "Reintegro realizado"}
          //ahora actualizo los datos en movimientos
        }else{
          var response = {
            "msg" : "Cuenta no encontrada"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}

function transferMov(req, res) {
  console.log('POST ' + config.base_url + '/transfer/:userid');
  console.log(" >> BODY:");
  console.log(req.body);
  console.log(" >> PARAMS:");
 console.log(req.params);

  //dado un IBAN A y uno B, obtengo saldo de A , resto importe y envio a una cuenta B
  let now= hoyFecha();

  var to = {
    "iban": req.body.Destino,
    "Concepto": req.body.Concepto,
    "Importe": req.body.Importe,
  }

  var id = req.body.Origen;
  var query = 'q={"iban": "'+id + '"}';
  console.log("La consulta es:" + query);

  var httpClient = requestJson.createClient(config.mlab.database_uri);  //inicializo mi url de conexión a la BD
  console.log("Busqueda de saldo del la cuenta");

  httpClient.get("account?" + query + "&" + config.mlab.database_fieldId + "&" + config.mlab.database_apikey, //aqui le indico la colección y la apikey
    function(err,resMLab, myaccount){          //para asegurar lo que estoy preguntando puedo hacer un console del resMLab
      if (err){
        var response = {
          "msg" : "Error obteniendo dados de la cuenta"
        }
        res.status(500);
      }else{ // actualizo el saldo
        if (myaccount.length > 0){
          console.log("Saldo de la cuenta:" + myaccount[0].balance);
          myaccount[0].balance = Number.parseInt(myaccount[0].balance) - Number.parseInt(req.body.Importe);
          console.log("Nuevo saldo de la cuenta:" + myaccount[0].balance);
          var putBody = '{"$set":{"balance":' + myaccount[0].balance+'}}';

          httpClient.put("account?" + query + "&" + config.mlab.database_apikey, JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT) {
              if (err){
                console.log(err);
                var response = {
                  "msg" : "Error grabando movimientos"
                }
              }else {
                var response = {
                  "msg" : "Reintegro registrado"
                }
                var newMov ={
                  "IBAN": req.body.Origen,
                  "Fecha": now,
                  "Concepto": req.body.Concepto,
                  "Importe": req.body.Importe,
                  "Saldo": myaccount[0].balance
                }

                httpClient.post ("movement?" + config.mlab.database_apikey, newMov,
                  function (err,resMLab, pasbody){
                    if (err){
                      console.log(err);
                      var response = {
                        "msg" : "Error grabando movimientos"
                      }
                    }
                  }
                );
                //envio a la otra cuenta
              }
            }
          );
          var response = {  "msg" : "Transferencia realizada"}
          ///ahora actualizo los datos en movimientos
        }else{
          var response = {
            "msg" : "Cuenta no encontrada"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}

function getSaldo(req, res){
  console.log('GET ' + config.base_url + '/accounts/:iban');
  console.log(" >> PARAMS:");
  console.log(req.params);

//  var userid = req.params.userid;
  var query = 'q={"iban": "'+req.params.iban + '"}';
  console.log("La consulta es:" + query);

  var httpClient = requestJson.createClient(config.mlab.database_uri);  //inicializo mi url de conexión a la BD
  console.log("Busqueda de saldo del la cuenta");

  httpClient.get("account?" + query + "&" + config.mlab.database_fieldId + "&" + config.mlab.database_apikey, //aqui le indico la colección y la apikey
    function(err,resMLab, myaccount){          //para asegurar lo que estoy preguntando puedo hacer un console del resMLab
      if (err){
        var response = {
          "msg" : "Error obteniendo dados de la cuenta"
        }
        res.status(500);
      }else{ // obtengo el saldo
        if (myaccount.length > 0){
          console.log("Saldo de la cuenta:" + myaccount[0].balance);
          var response =  myaccount;        
        }else{
          var response = {
            "msg" : "Cuenta no encontrada"
          }
      //    res.status(404);
        }
      }
      res.send(response);
    }
  );
}
function hoyFecha(){
  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1;
  var yyyy = hoy.getFullYear();

  dd = addZero(dd);
  mm = addZero(mm);

  return dd+'/'+mm+'/'+yyyy;
}

function addZero(i) {
  if (i < 10) {
      i = '0' + i;
  }
  return i;
}

module.exports.ingresoMov = ingresoMov;
module.exports.reintegroMov = reintegroMov;
module.exports.transferMov = transferMov;
module.exports.getSaldo = getSaldo;
