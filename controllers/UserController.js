const config = require('../libs/config');
const crypt = require('../libs/crypt'); // representa nuestra librería de criptografía
const requestJson = require('request-json');

function getUsers(req, res){
  console.log('GET ' + config.base_url + '/users');
  console.log(" >> QUERY STRING:");
  console.log(req.query);

  var params = req.query;

  var httpClient = requestJson.createClient(config.mlab.database_uri);
  console.log(" >> Cliente http creado");
  httpClient.get("user?" + config.mlab.database_fieldId + "&" + config.mlab.database_apikey,
    function(err, resMLab, body) {
      // console.log(resMLab);

      if (err) {
        var respuesta = {
          "msg": "Error obteniendo usuarios"
        }
        res.status(500);
      } else {
        var respuesta = {};

        if (params.$count=="true") {
          console.log("Nº registros users = " + body.length);
          respuesta.count = body.length;
        }

        respuesta.users = params.$top ? body.slice(0, params.$top) : body;
      }

      res.send(respuesta);
    }
  );
}

function getUsersById(req, res){
  console.log('GET ' + config.base_url + '/users/:userid');
  console.log(" >> PARAMS:");
  console.log(req.params);

  var id = req.params.userid;
  var query = 'q={"id":"' + id + '"}';
  console.log(" >> Consulta = " + query);

  var httpClient = requestJson.createClient(config.mlab.database_uri);
  console.log(" >> Cliente http creado");
  httpClient.get("user?" + query + "&" + config.mlab.database_fieldId + "&" + config.mlab.database_apikey,
    function(err, resMLab, body) {
      // console.log(resMLab);

      if (err) {
        var respuesta = {
          "msg": "Error obteniendo usuario"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var respuesta = body[0];
        } else {
          var respuesta = {
            "msg": "Usuario no encontrado"
          }
          res.status(404);
        }
      }

      res.send(respuesta);
    }
  );
}

function createUser(req, res){
  console.log('POST ' + config.base_url + '/users');
  console.log(" >> BODY:");
  console.log(req.body);

  const userId = crypt.encryptStringAES(req.body.email + req.body.first_name + req.body.last_name);

  var newUser = {
    "id": userId,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": crypt.hash(req.body.password),
    "type": req.body.type,
    "home": {
      "country": req.body.home.country,
      "province":req.body.home.province,
      "location":req.body.home.location,
      "address":req.body.home.address,
      "postal_code":req.body.home.postal_code,
      "geolocation": {
        "longitude": req.body.home.geolocation.longitude,
        "latitude": req.body.home.geolocation.latitude
      }
    }
  };
  
  console.log(" >> newUser:");
  console.log(newUser);

  var httpClient = requestJson.createClient(config.mlab.database_uri);
  console.log(" >> Cliente http creado");

  // Validamos que el usuario no exista previamente
  var query = 'q={"email":"' + req.body.email + '"}';
  console.log(" >> Consulta = " + query);

  httpClient.get("user?" + query + "&" + config.mlab.database_fieldId + "&" + config.mlab.database_apikey,
    function(err, resMLab, body) {
      // console.log(resMLab);

      if (err) {
        var respuesta = {
          "msg": "Error validando usuario"
        }
        res.status(500).send(respuesta);
      } else {
        if (body.length > 0) {
          console.log(body);
          var respuesta = {
            "msg": "Usuario existente"
          }
          res.status(409).send(respuesta);
        } else {
          httpClient.post("user?" + config.mlab.database_apikey, newUser,
            function(err, resMLab, body) {
              console.log("Usuario guardado");
              console.log(body);

              var respuesta = {};

              respuesta.msg = "Usuario creado";
              respuesta.id = userId;

              res.status(201).send(respuesta);
            }
          );
        }
      }
    }
  );
}

function deleteUserById(req, res){
  console.log('DELETE ' + config.base_url + '/users/:userid');
  console.log(" >> PARAMS:");
  console.log(req.params);

  var id = req.params.userid;
  var query = 'q={"id":"' + id + '"}';
  console.log(" >> Consulta = " + query);
  var userDeleted = [];

  var httpClient = requestJson.createClient(config.mlab.database_uri);
  console.log(" >> Cliente http creado");
  httpClient.put("user?" + query + "&" + config.mlab.database_apikey, userDeleted,
    function(err, resMLab, body) {
      //console.log(resMLab);
      console.log(body);
      if (err) {
        var respuesta = {
          "msg": "Error obteniendo usuario"
        }
        res.status(500);
      } else {
        if(body.removed>0) {
          var respuesta = {
            "msg": "Usuario borrado"
          }
        } else {
          var respuesta = {
            "msg": "Usuario no encontrado"
          }
          res.status(404);
        }
      }
      res.send(respuesta);
    }
  );
}

module.exports.getUsers = getUsers;
module.exports.getUsersById = getUsersById;
module.exports.createUser = createUser;
module.exports.deleteUserById = deleteUserById;
