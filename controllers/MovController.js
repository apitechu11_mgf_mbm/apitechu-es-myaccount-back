const config = require('../libs/config');
const crypt = require('../libs/crypt'); // representa nuestra librería de criptografía
const requestJson = require('request-json');

function consultarMov(req, res) {
  console.log('GET ' + config.base_url + '/movement/:userid/:iban');
  console.log(" >> PARAMS:");
  console.log(req.params);

  var id = req.params.iban;
  var query = 'q={"IBAN": "' + id + '"}';
  console.log("La consulta es:" + query);

  var httpClient = requestJson.createClient(config.mlab.database_uri);  //inicializo mi url de conexión a la BD
  console.log("Busqueda de movimientos");

  httpClient.get("movement?" + query + "&" + config.mlab.database_fieldId + "&" + config.mlab.database_apikey,
   function(err,resMLab, body){          //para asegurar lo que estoy preguntando puedo hacer un console del resMLab
      if (err){
        var response = {
          "msg" : "Error obteniendo movimientos"
        }
        res.status(500);
      }else{
        if (body.length > 0){
          var response = body;
        }else{
          var response = {
            "msg" : "Cuenta no encontrada"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}

module.exports.consultarMov = consultarMov;
