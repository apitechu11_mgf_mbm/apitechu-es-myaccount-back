const config = require('../libs/config');
const ibanLib = require('../libs/iban');
const crypt = require('../libs/crypt'); // representa nuestra librería de criptografía
const requestJson = require('request-json');

function getAccounts(req, res){
  console.log('GET ' + config.base_url + '/accounts');
  console.log(" >> QUERY STRING:");
  console.log(req.query);

  var params = req.query;

  var httpClient = requestJson.createClient(config.mlab.database_uri);
  console.log(" >> Cliente http creado");
  httpClient.get("account?" + config.mlab.database_fieldId + "&" + config.mlab.database_apikey,
    function(err, resMLab, body) {
      // console.log(resMLab);

      if (err) {
        var respuesta = {
          "msg": "Error obteniendo cuentas"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var respuesta = {};
          if (params.$count=="true") {
            console.log("Nº registros cuentas = " + body.length);
            respuesta.count = body.length;
          }

          respuesta.accounts = params.$top ? body.slice(0, params.$top) : body;
        } else {
          var respuesta = {
            "msg": "no hay cuentas"
          }
          res.status(404);
        }
      }
      res.send(respuesta);
    }
  );
}

function getAccountsById(req, res){
  console.log('GET ' + config.base_url + '/accounts/:userid');
  console.log(" >> PARAMS:");
  console.log(req.params);

  var userid = req.params.userid;
  var query = 'q={"userId":"' + userid + '"}';
  console.log(" >> Consulta = " + query);

  var httpClient = requestJson.createClient(config.mlab.database_uri);
  console.log(" >> Cliente http creado");

  httpClient.get("account?" + query + "&" + config.mlab.database_fieldId + "&" + config.mlab.database_apikey,
    function(err, resMLab, body) {
      // console.log(resMLab);
      if (err) {
        var respuesta = {
          "msg": "Error obteniendo cuentas"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var respuesta = body;
        } else {
          var respuesta = {
            "msg": "no hay cuentas"
          }
          res.status(404);
        }
      }

      res.send(respuesta);
    }
  );
}

function createAccount(req, res) {
  console.log('POST ' + config.base_url + '/accounts/:userid');
  console.log(" >> PARAMS:");
  console.log(req.params);
  console.log(" >> BODY:");
  console.log(req.body);

  const userId = req.params.userid;
  const iban = ibanLib.formatear(ibanLib.calcular(req.body.entidad + '-' + req.body.oficina + '-??-' + generaCuenta()));
  var accountId = crypt.encryptStringAES(iban);

  var newAccount = {
    "userId": userId,
    "id": accountId,
    "iban": iban, 
    "alias": req.body.alias,
    "divisa": req.body.divisa,
    "balance": 0
  };

  console.log(" >> newAccount:");
  console.log (newAccount);

  var httpClient = requestJson.createClient(config.mlab.database_uri);
  console.log(" >> Cliente http creado");

  httpClient.post("account?" + config.mlab.database_apikey, newAccount,
    function (err,resMLab, body){
      if(err){
        var response = {
          "msg" : "Error validando cuenta"
        }
        res.status(500).send(response);
        console.log(err);
      }else{
        console.log("cuenta creada");
        console.log(body);
        var respuesta = {};

        respuesta.msg = "Cuenta de cliente creada";
        respuesta.iban = iban;
        res.status(201).send(respuesta);
      }
    }
  )
}


function deleteAccountById(req, res) {
  console.log('DELETE ' + config.base_url + '/accounts/:userid/:iban');
  console.log(" >> PARAMS:");
  console.log(req.params);

  //var query = 'q={"userid": "'+ req.params.userid + '"}';
  var query = 'q={"iban": "'+ req.params.iban + '"}';
  console.log("La consulta es:" + query);
  var delAccount = [];

  var httpClient = requestJson.createClient(config.mlab.database_uri);
  console.log(" >> Cliente http creado");

  httpClient.put("account?" + query + "&" + config.mlab.database_apikey, delAccount,
    function(err,resMLab, body){
      console.log(body);
      if (err) {
        var respuesta = {
          "msg": "Error elminando la cuenta"
        }
        res.status(500);
      } else {
        if(body.removed>0) {
          var respuesta = {
            "msg": "Cuenta eliminada"
          }
          res.status(200);
        } else {
          var respuesta = {
            "msg": "Cuenta no encontrada"
          }
          res.status(404);
        }
      }
      res.send(respuesta);
    }
  )
}

function generaCuenta() {
  var newcuenta ="";
  for ( var i = 0; i < 10; i++) {
    newcuenta += Math.floor(Math.random() * 10);
  }
  return newcuenta;
};

module.exports.getAccounts = getAccounts;
module.exports.getAccountsById = getAccountsById;
module.exports.createAccount = createAccount;
module.exports.deleteAccountById = deleteAccountById;
