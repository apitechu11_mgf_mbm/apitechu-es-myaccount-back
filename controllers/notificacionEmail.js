const config = require('../libs/config');
const requestJson = require('request-json');
const crypt = require('../libs/crypt'); // representa nuestra librería de criptografía
var nodemailer = require('nodemailer'); // email sender function exports.sendEmail = function(req, res){

var transporter = nodemailer.createTransport({
       service: 'Gmail',
       auth: {
           user: config.mailserver,
           pass: config.passserver
       }
});

var mailOptions = {
       from: 'My Bank',
       to: 'mercedes.garciaf@gmail.com',
       subject: 'Cambio de password de MyBank',
       text: 'Contenido del email'
};

// transporter.sendMail(mailOptions, function(error, info){
//     if (error){
//         console.log(error);
//     //    res.send(500, err.message);
//     } else {
//         console.log("Email sent");
//     //    res.status(200).jsonp(req.body);
//     }
// });

function sendForget(req, res){
  console.log('POST ' + config.base_url + '/forgetpasswd');
  console.log(" >> BODY:");
  console.log(req.body);

  var email = req.body.email;
  var query = 'q={"email":"' + email + '"}';
  console.log(" >> Consulta = " + query);

  var httpClient = requestJson.createClient(config.mlab.database_uri);

  httpClient.get("user?" + query + "&" + config.mlab.database_apikey,
    function(err, resMLab, body) {
      // console.log(resMLab);
      console.log(body);
      var respuesta = {};
      var contraseña = "";
      var caracteres = "abcdefghijkmnpqrtuvwxyzABCDEFGHIJKLMNPQRTUVWXYZ2346789";
      for (i=0; i<10; i++){
        contraseña += caracteres.charAt(Math.floor(Math.random()*caracteres.length));
      }

      if (body.length > 0) {
        console.log("Usuario a resetear contraseña existe")
        var mailOptions = {
               from: 'MyBank',
               to: req.body.email,
               subject: 'Cambio de password de MyBank',
               text: 'Su nueva password para acceso a MyAccount es:  '+ contraseña
        };
        console.log(contraseña);
        transporter.sendMail(mailOptions, function(error, info){
              if (error){
                  console.log(error);
                  res.send(500, err.message);
              } else {
                  console.log("Email sent");
                //  res.status(200).jsonp(req.body);
                  var token = crypt.hash(contraseña);
                  var putBody = '{"$set":{"password":"' + token +'"}}';

                    httpClient.put("user?" + query + "&" + config.mlab.database_apikey, JSON.parse(putBody),
                      function(errBody, resMLab, bodyPut) {
                        console.log(bodyPut);
                        res.status(200).jsonp(req.body);
                      }
                    );

              }
          });


        } else {
          console.log("usuario de reseteo no existe");
          var msg = "Usuario no existente";
          respuesta.msg = msg;
          res.status(401).send(respuesta);
        }
      }
    );
}

module.exports.sendForget = sendForget;
