# Proyecto TechU - edición 11 - Merche y Marco

App Backend del proyecto

## prerequisitos donde instalar el proyecto back myAccount

Aseguráte que tienes instalado los siguientes paquetes de SW:
 - npm
 - node
 - git

En caso de no tenerlos puedes obtenerlos del siguiente modo:

```
$ sudo apt-get update
$ sudo apt-get install nodejs
$ sudo apt-get install npm
$ sudo apt-get install git
```

## Descargar la aplicación

```
$ cd ../projects/myaccount/
$ git clone https://bitbucket.org/apitechu11_mgf_mbm/apitechu-es-myaccount-back.git
```

## Obtener las dependencias del proyecto

```
$ cd apitechu-es-myaccount-back
$ npm install
```

Este comando te creará la carpeta /node_modules/ con las delendencias del fichero package.json

## Definir las variables de entorno del proyecto 

```
$ touch .env
```
Editar el fichero y completar las siguientes variables
 - MLAB_ID_CONTAINER=valor del projecto de MLAB
 - MLAB_API_KEY=valor de la clave de acceso de ese projecto
 - PORT=3000
 - CRYPTO_JWT_SECRET=clave cifrado jwt del proyecto
 - CRYPTO_AES_SECRET=clave cifrado aes del proyecto

## Arrancar el servidor con las apis del proyecto

```
$ node server
```

## Ejecutar los Tests del proyecto

```
$ npm run test
```
